import { Component, OnInit } from "@angular/core";
import { AdMobFree, AdMobFreeBannerConfig } from "@ionic-native/admob-free/ngx";
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';

@Component({
  selector: "app-home",
  templateUrl: "./home.page.html",
  styleUrls: ["./home.page.scss"],
})
export class HomePage implements OnInit {
  constructor(private admobFree: AdMobFree,private iab: InAppBrowser) {}

  access(){
    this.iab.create("http://par.hrmsodisha.gov.in/index.htm")
  }

  init() {
    const bannerConfig: AdMobFreeBannerConfig = {
      // add your config here
      // for the sake of this example we will just use the test config
      isTesting: true,
      // id: 'ca-app-pub-4915431854425026/9935141409',
      autoShow: true,
    };
    this.admobFree.banner.config(bannerConfig);

    this.admobFree.banner
      .prepare()
      .then(() => {
        // banner Ad is ready
        // if we set autoShow to false, then we will need to call the show method here
      })
      .catch((e) => console.log(e));
  }

  ngOnInit() {
    this.init();
  }
}
